from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()

from sqlalchemy import Column, Integer, String, Float, DateTime, Enum, Sequence, create_engine

class Records(Base):
    __tablename__ = 'records'
    id = Column(Integer, Sequence('records_id_seq'), primary_key=True)
    date = Column(DateTime())
    latitude = Column(Float())
    longitude = Column(Float())
    temperature = Column(Float())

    def get_serializable(self):
        return {
            'id': self.id,
            'date': '%s'%self.date,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'temperature': self.temperature
            }

    def __repr__(self):
        return "<Records(date='%s', lat='%s', lon='%s', temp='%s')>" % (
                                self.date, self.latitude, self.longitude, self.temperature)
    
    
import enum
class TripMode(enum.Enum):
    foot = 1
    bike = 2
    car  = 3
    boat = 4

class Trips(Base):
    __tablename__ = 'trips'
    id = Column(Integer, Sequence('trips_id_seq'), primary_key=True)
    title = Column(String(50))
    mode = Column(Enum(TripMode))
    
    datestart = Column(DateTime())
    datestop = Column(DateTime())
    
    latitude_start = Column(Float())
    longitude_start = Column(Float())
    
    latitude_stop = Column(Float())
    longitude_stop = Column(Float())


    def get_serializable(self):
        return {
            'id': self.id,
            'title': self.title,
            'mode': '%s'%self.mode,
            'datestart': '%s'%self.datestart,
            'datestop': '%s'%self.datestop,
            
            'latitude_start': self.latitude_start,
            'longitude_start': self.longitude_start,
            
            'latitude_stop': self.latitude_stop,
            'longitude_stop': self.longitude_stop,
            
            }

    def __repr__(self):
        return "<Trips(id='%s', title='%s', mode='%s')>" % (
                                self.id, self.title, self.mode)
    
    
    
class Medias(Base):
    __tablename__ = 'medias'
    id = Column(Integer, Sequence('medias_id_seq'), primary_key=True)
    title = Column(String(50))
    date = Column(DateTime())
    
    description = Column(String(50))
    path = Column(String(50))
    
    latitude = Column(Float())
    longitude = Column(Float())

    def get_serializable(self):
        return {
            'id': self.id,
            'title': self.title,
            'date': '%s'%self.date,
            'description': self.description,
            'path': self.path,
            'latitude': self.latitude,
            'longitude': self.longitude,
            
            }

    def __repr__(self):
        return "<Medias(id='%s', title='%s')>" % (
                                self.id, self.title)
    

class Users(Base):
    __tablename__ = 'users'
    id = Column(Integer, Sequence('users_id_seq'), primary_key=True)
    email = Column(String(50))
    password = Column(String(50))
    
    def __repr__(self):
        return "<Users(id='%s', email='%s')>" % (
                                self.id, self.email)
