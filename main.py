#!/usr/bin/env python3

#sudo apt install python3-sqlalchemy
from src.database import *
from pychipfox.chipfox import Chipfox
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import argparse
from datetime import datetime
import time
import os
import threading
import uuid
import re
from flask_cors import CORS
from flask import Flask, render_template, send_from_directory, request, Response, session, redirect
import hashlib
import requests
import piexif
from PIL import Image
import json

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--uri", help="gps tracker uri (ie chipfox://user:password")
    parser.add_argument("-P", "--port", help="http port", default='80')
    parser.add_argument("-d", "--database", help="database", default='database.db')
    parser.add_argument("-m", "--mediaspath", help="media path", default='./medias/')
    parser.add_argument("-i", "--init", action="store_true", help="import all records")
    return parser.parse_args()

class Simu(threading.Thread):
    def __init__(self, path):
        threading.Thread.__init__(self)
        self.daemon = True
        self.data = json.loads(open(path, 'r').read())
        self.cb = None
        self.start()
    
    def register_update_callback(self, cb):
        self.cb = cb
    
    def run(self):
        for d in self.data:
            if self.cb is not None:
                data = {
                    'time':datetime.now().timestamp(),
                    'computedLocation': {
                            'lat': d['latitude'],
                            'lng': d['longitude'],
                        },
                    'payload': {'temperature': d['temperature']}
                }
                self.cb(data)
            time.sleep(10)
        

class App:
    def __init__(self, database, mediaspath, tracker_uri):
        engine = create_engine('sqlite:///%s?check_same_thread=False'%database, echo = False)
        Base.metadata.create_all(engine)
        self.mediaspath = mediaspath
        self.flask = Flask(__name__)
        CORS(self.flask)
        self.flask.config['SECRET_KEY'] = 'wtf?'

        self.lock = threading.Lock()

        Session = sessionmaker()
        Session.configure(bind=engine)
        self.session = Session()
        self.tracker = None
        
        if 'chipfox' in tracker_uri:
            user, password = tracker_uri.replace('chipfox://', '').split(':')
            self.tracker = Chipfox(user, password)
            trackers = self.tracker.fetch_trackers_list()
            for _, tracker in trackers.items():
                print('registering tracker %s'%tracker)
                tracker.register_update_callback(self.on_position_update)
        elif 'file' in tracker_uri:
            path = tracker_uri.replace('file://', '')
            self.tracker = Simu(path)
            self.tracker.register_update_callback(self.on_position_update)
        
        self.flask.add_url_rule('/api/trips', methods=['GET'], view_func=self.trips)
        self.flask.add_url_rule('/api/trips/<action>/<id>', methods=['GET'], view_func=self.actions_trips)
        self.flask.add_url_rule('/api/trips/add', methods=['GET', 'POST'], view_func=self.trips_add)
        
        self.flask.add_url_rule('/api/records/<action>/<id>', methods=['GET'], view_func=self.record_action)
        
        self.flask.add_url_rule('/api/position', methods=['GET'], view_func=self.position)
        self.flask.add_url_rule('/api/status', methods=['GET'], view_func=self.status)
        
        self.flask.add_url_rule('/api/login', methods=['POST'], view_func=self.login)
        self.flask.add_url_rule('/api/logout', methods=['POST', 'GET'], view_func=self.logout)
        
        self.flask.add_url_rule('/api/medias/upload', methods=['POST'], view_func=self.upload)
        
        self.flask.add_url_rule('/api/medias/<action>/<id>', methods=['GET', 'POST'], view_func=self.medias_actions)
        
        
        
        self.flask.run(host='0.0.0.0', port=args.port)

    def login(self):
        password = hashlib.sha256(request.form['password'].encode('utf-8')
).hexdigest()
        results = self.session.query(Users).filter_by(email=request.form['email']).first()#.filter_by(password=password).first()
        if results is not None:
            print('%s logged'%results)
            session['email'] = request.form['email']
            return Response(response=json.dumps('logged as %s'%results.email),
                status=200,
                mimetype="application/json")
        else:
            print('No user found for email %s'%request.form['email'])
            return Response(response=json.dumps('invalid credentials'),
            status=403,
            mimetype="application/json")

    def logout(self):
        session['email'] = None
        return Response(response=json.dumps('OK'),
                status=200,
                mimetype="application/json")

    def record_action(self, action, id):
        print('Record action %s %s'%(action, id))
        if action == 'delete':
            with self.lock:
                self.session.query(Records).filter_by(id=id).delete()
                self.session.commit()
        return Response(response=json.dumps('ok'),
            status=200,
            mimetype="application/json")

    def position(self):
        with self.lock:
            result = self.session.query(Records).order_by(Records.date.desc()).first()
        return Response(response=json.dumps(result.get_serializable()),
            status=200,
            mimetype="application/json")

    def trips(self):
        with self.lock:
            results = self.session.query(Trips).all()
        data = []
        for r in results:
            data.append(
                r.get_serializable()
                )
        return Response(response=json.dumps(data),
            status=200,
            mimetype="application/json")
    
    def status(self):
        
        email = None
        if "email" in session:
            email = session['email']
        return Response(response=json.dumps({
            'user': email
            }),
            status=200,
            mimetype="application/json")
    
    def actions_trips(self, action, id):
        if action == 'view':
            return self.trip_view(id)
        elif action == 'finish':
            with self.lock:
                r = self.session.query(Trips).filter_by(id=id).first()
                r.datestop = datetime.now()
                r = self.session.add(r)
                self.session.commit()
        return Response(response=json.dumps('OK'),
            status=200,
            mimetype="application/json")
    
    def trip_view(self,id):
        with self.lock:
            trip = self.session.query(Trips).filter_by(id=id).first()
        data = trip.get_serializable()
        data['records_raw'] = []

        req = self.session.query(Records).filter(Records.date>=trip.datestart)
        
        if trip.datestop is not None:
            req.filter(Records.date<=trip.datestop)
        with self.lock:
            raw = req.all()
        
        diff=0
        if len(raw) > 1:
            diff = (raw[0].date - raw[-1].date).total_seconds()
            data['duration'] = diff
        
        
        request_parameters = []
        for r in raw:
            request_parameters.append('%s,%s'%(r.longitude, r.latitude))
            data['records_raw'].append(
                r.get_serializable()
                )
            
        
        req = self.session.query(Medias).filter(Medias.date>=trip.datestart)
        
        if trip.datestop is not None:
            req.filter(Medias.date<=trip.datestop)
        with self.lock:
            medias = req.all()
        
        data['medias'] = []
        for m in medias:
            data['medias'].append(m.get_serializable())
            
            
        
        url= 'https://router.project-osrm.org/route/v1/foot/'+';'.join(request_parameters)+'?geometries=geojson&alternatives=false&steps=true&generate_hints=false'
        
        try:
            resp = requests.get(url)
            response = resp.json()
            data['records'] = response['routes'][0]['geometry']['coordinates']
            data['distance'] = response['routes'][0]['distance']
        except Exception as e:
            print('Could not fetch route :%s'%e)
        
        return Response(response=json.dumps(data),
            status=200,
            mimetype="application/json")

    def on_position_update(self, r):
        rc = Records(date=datetime.fromtimestamp(int(r['time'])), latitude=r['computedLocation']['lat'], longitude=r['computedLocation']['lng'], temperature=r['payload']['temperature'])
        print('position_update %s'%rc.date)
        with self.lock:
            self.session.add(rc)
            self.session.commit()
    
    def allowed_file(self, filename):
        return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ['jpg', 'png', 'jpeg']
    
    def create_media_thumbnail(self, filename):
        inpath = os.path.join(self.mediaspath, filename)
        outpath = os.path.join(self.mediaspath, 'lq_'+filename)
        basewidth = 300
        img = Image.open(inpath)
        wpercent = (basewidth/float(img.size[0]))
        hsize = int((float(img.size[1])*float(wpercent)))
        img = img.resize((basewidth,hsize), Image.ANTIALIAS)
        img.save(outpath)
    
    def upload(self):
        if request.method == 'POST':
            # check if the post request has the file part
            title = request.values['title']
            description = request.values['description']
            tripid = request.values['tripid']
            if 'file' not in request.files:
                return Response(response=json.dumps('no file part'),
                    status=404,
                    mimetype="application/json")
            file = request.files['file']
            filename = '%s.%s'%('%s'%uuid.uuid4(), file.filename.rsplit('.', 1)[1].lower())
            if file and self.allowed_file(file.filename):
                path = os.path.join(self.mediaspath, filename)
                file.save(path)
                self.create_media_thumbnail(filename)
                if self.register_image(tripid, title, description, path):
                    return Response(response=json.dumps('OK'),
                            status=200,
                            mimetype="application/json")
                else:
                    os.remove(path)
                    return Response(response=json.dumps('could not register image'),
                        status=404,
                        mimetype="application/json")
            else:
                return Response(response=json.dumps('invalid extension'),
                        status=404,
                        mimetype="application/json")
    
    def medias_actions(self, action, id):
        if 'view' in action:
            return self.view_media(id, action=='preview')
        elif action == 'delete':
            with self.lock:
                r = self.session.query(Medias).filter_by(id=id).first()
                path = os.path.join(self.mediaspath, r.path)
                self.session.query(Medias).filter_by(id=id).delete()
                self.session.commit()
            os.remove(path)
        elif action == 'update':
            with self.lock:
                r = self.session.query(Medias).filter_by(id=id).first()
                r.latitude = request.values['latitude']
                r.longitude = request.values['longitude']
                r = self.session.add(r)
        return Response(response=json.dumps('OK'),
                    status=200,
                    mimetype="application/json")
    
    def get_media(self, filename):  # pragma: no cover
        try:
            src = os.path.join(self.mediaspath, filename)
            # Figure out how flask returns static files
            # Tried:
            # - render_template
            # - send_file
            # This should not be so non-obvious
            return open(src, 'rb').read()
        except IOError as exc:
            return str(exc)
    
    def view_media(self,id, preview=False):
        with self.lock:
            result = self.session.query(Medias).filter_by(id=id).first()
        path=result.path
        
        if preview:
            path = 'lq_'+path
        
        mimetypes = {
            ".jpg": "image/jpeg",
            ".png": "image/png",
        }
        ext = os.path.splitext(path)[1]
        mimetype = mimetypes.get(ext, "image/png")
        content = self.get_media(path)
        return Response(content, mimetype=mimetype)
    
    def get_date_from_exif(self, exif_dict):
        try:
            date_obj = datetime.strptime(str(exif_dict['Exif'].get(36867).decode()), '%Y:%m:%d %H:%M:%S')
            return date_obj
        except Exception as e:
            print('EXIF error %s'%(e))
        return None
    
    def get_lat_lon_from_exif(self, exif_dict):
        def to_decimal(exifgpsdata):
            def _p(arr):
                return arr[0]/arr[1]
            return _p(exifgpsdata[0])  + _p(exifgpsdata[1])/60. + _p(exifgpsdata[2])/3600.
        return to_decimal(exif_dict['GPS'][2]), to_decimal(exif_dict['GPS'][4])
    
    def get_metadata_from_estimation(self, tripid):
        with self.lock:
            trip = self.session.query(Trips).filter_by(id=tripid).first()
            raw = self.session.query(Records).filter(Records.date>=trip.datestart).filter(Records.date<=trip.datestop).first()
        return raw.date, raw.latitude, raw.longitude
    
    def register_image(self, tripid, title, descripion, path):
        try:
            exif_dict = piexif.load(path)
            try:
                date = self.get_date_from_exif(exif_dict)
                lat,lon = self.get_lat_lon_from_exif(exif_dict)
            except:
                date, lat, lon = self.get_metadata_from_estimation(tripid)
            
            with self.lock:
                m = Medias(date=date, title=title, description=descripion, path=os.path.basename(path), latitude=lat, longitude=lon)
                self.session.add(m)
                self.session.commit()
        except Exception as e:
            print('Could not register image %s'%e)
            return False
        return True
    
    
    def trips_add(self):
        dte = request.values['datestart']
        if dte == '':
            dte = datetime.now()
        t = Trips(
                    title=request.values['title'],
                    mode=request.values['mode'],
                    datestart=dte,
                    #datestop=request.values['datestop'],
                  )
        with self.lock:
            self.session.add(t)
            self.session.commit()
        return Response(response=json.dumps('OK'),
                    status=200,
                    mimetype="application/json")

def main(args):

    app = App(args.database, args.mediaspath, args.uri)
    if args.init:
        trackers = app.chipfox.fetch_trackers_list()
        for _, tracker in trackers.items():
            print(f'{tracker}')
            records = tracker.fetch_all_locations()
            for r in records:
                rc = Records(date=datetime.fromtimestamp(int(r['time'])), latitude=r['computedLocation']['lat'], longitude=r['computedLocation']['lng'], temperature=r['payload']['temperature'])
                app.session.add(rc)
        app.session.commit()
    
if __name__ == '__main__':
    args = parse_args()
    args.port = int(args.port)
    main(args)
